from pathlib import Path

from zipfile import ZipFile
from py7zr import SevenZipFile

class archive():
    def extract(self, archive_file: Path, extraction_dir: Path, debug = 0):
        if debug > 0: print("Started extraction")
        if debug > 1: print(archive_file)
        if debug > 2: print("suffix:", archive_file.suffix, type(archive_file.suffix))

        if archive_file.suffix == ".zip":
            with ZipFile(archive_file, 'r') as zip_file:
                zip_file.extractall(extraction_dir)
        elif archive_file.suffix in [".7z", ".7zip"]:
            with SevenZipFile(archive_file, 'r') as zip_file:
                zip_file.extractall(extraction_dir)

        else:
            raise NotImplementedError("Can't work with archives of type " + archive_file.suffix + " (yet)")