import json
from pathlib import Path

settings_debug = False

class file_manager():
    def __init__(self, main_file: Path):
        self.main_file = main_file

        if not main_file.exists():
            default_file = (main_file.stem + "_default" + main_file.suffix)
            print(default_file)
            self.main_file.write_bytes(Path(default_file).read_bytes())
        elif main_file.is_dir():
            raise RuntimeError("A folder named with a main file name, please remove!")


    def get_all_files(self, from_full_path: Path) -> list[Path]:
        return [f for f in from_full_path.rglob("*") if f.is_file()]


    def get_dict(self) -> dict:
        with open(self.main_file, "r") as f:
            result = json.load(f)
        
        return result


    def remove_empty_dirs(self, dir:Path, print_out: bool = False):
        if settings_debug:
            print_out = True

        # collect all dirs in current dir
        dirs = [d for d in dir.iterdir() if d.is_dir()]

        for d in dirs:
            # run this method for every dir in the current dir,
            # cleaning up every empty dir in the tree
            self.remove_empty_dirs(d)

        # check if any path objects are left
        p_objs = [x for x in dir.iterdir()]
        
        # if not, remove the dir, as it is empty
        if len(p_objs) == 0:
            if print_out: print("RMD", dir)
            dir.rmdir()


    def rm_rf(self, dir: Path, print_out: bool = False, i_know = False):
        # this method intentially NOT first cleaning up files then use remove empty folders method,
        # because that would be much slower, making and iterating list over and over again

        if not i_know:
            print("WARNING! This method is removing all files and folders, without any confirmation!")
        if settings_debug:
            print_out = True

        # collect all files and folders
        p_objects = [x for x in dir.rglob("*")]
        # remove one by one from end to start:
        for p_obj in reversed(p_objects):
            if p_obj.is_file():
                if print_out: print("DEL", p_obj)
                p_obj.unlink()
            elif p_obj.is_dir():
                if print_out: print("RMD", p_obj)
                p_obj.rmdir()
            else:
                raise RuntimeError("Neither file nor dir:", p_obj)


    def save_dict(self, dic: dict):
        with open(self.main_file, "w") as f:
            json.dump(dic, f, indent=2)