#from pathlib import Path
from datetime import datetime

#from file_operations import file_manager as files

#patches_folder = "knowledge_patches"
latest_patch = 2 # not 2. 2 is in dev. sorta...




class patcher():
    def __init__(self, files_i):
        self.files = files_i

    def get_latest_knowledge_patch_version(self):
        # todo : read latest patch from folder
        return latest_patch

    def upgrade_knowledge(self, to_version: int = None):
        knowledge = self.files.get_dict()
        if not to_version:
            to_version = self.get_latest_knowledge_patch_version()
        k_version = knowledge.get("version", 0)
        if k_version < to_version - 1:
            self.upgrade_knowledge(to_version - 1)
        if k_version == to_version - 1:
            self.apply(to_version)
            

    def apply(self, version):
        # should have done it with just several ifs inttead of recursion:
        # if version < 1: ...
        # if version < 2: ...
        # ...
        print("Applying knowledge patch", version)
        knowledge = self.files.get_dict()

        if version == 1:
            knowledge = self.apply1(knowledge)
        elif version == 2:
            knowledge = self.apply2(knowledge)
        # elif version == 3:
        #     knowledge = self.apply3(knowledge)
        else:
            raise NotImplementedError("Wrong version!")

        self.files.save_dict( knowledge )

    def apply1(self, dictionary: dict) -> dict:
        dictionary["version"] = 1
        return dictionary

    def apply2(self, dictionary: dict) -> dict:
        # add installed date
        # make it possible to have several mods use the same file
        dcopy = dictionary.copy()

        for file, mod_name in dictionary["installed_files"].items():
            dcopy["installed_files"][file] = {
                "mods": [mod_name],
                "installed": str(datetime.now())
                # can return with dt_obj = datetime.strptime(time_str, '%Y-%m-%dT%H:%M:%S%z')
            }

        dictionary = dcopy
        dictionary["version"] = 2
        return dictionary
        