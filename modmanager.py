from ctypes import WinError, resize
from datetime import datetime
from os import PathLike
from pathlib import Path
#from tempfile import TemporaryDirectory
import xml.etree.ElementTree as ET


from archive_manager import archive
from my_little_yaml import yaml_reader
from knowledge_patcher import patcher
from file_operations import file_manager


test_mode = False

settings_mod_manager_folder = Path(__file__).parent
settings_game_folder = ( settings_mod_manager_folder / ".." ).resolve()
settings_gamedata_folder = settings_game_folder / "gamedata"
mods_installed_folder = settings_mod_manager_folder / "mods installed" # not in use
settings_mods_to_install_folder = settings_mod_manager_folder / "mods to install"
settings_conf_folder = settings_mod_manager_folder / "conf"
settings_manager_installed_mods_file = settings_mod_manager_folder / "installed_mods.json"
settings_temp_dir = settings_mod_manager_folder / "TEMP" # need to be on the same drive as 

# the bigger the numer, the more debug you'll see
debug = 2

# only decorative:
decorations_width = 30

to_do = "\n\n" + " ToDo ".center(decorations_width, "=") + """

0. !!! Fix preinstall checks! Don't just overwrite as it does now!!
0.1 make install from here work outside gamedata
1. make ability to use patches for installed mods
1.1. folder with patched files,
1.2. folder with backups for each mod
1.3. on removal, either use original file from backup or load another patch for a mod combo
2. automated install (parse fomod or use yaml where not present)
3. add 'install before/after' feature
4. complete 'install all' feature"""

fat_separator = "#"*decorations_width
thin_separator = "-"*decorations_width


if test_mode:
    settings_gamedata_folder = ( settings_gamedata_folder / ".." / "gamedata.test" ).resolve()
    settings_manager_installed_mods_file = settings_manager_installed_mods_file.with_stem(settings_manager_installed_mods_file.stem + "_test")


def check_no_file_conflicts_old_code_using_to_test_without_install(folder_to: Path, file_list_relative: list):
    if debug > 1: print("check_no_file_conflicts")
    result = True
    conflicting_files = []

    for file in file_list_relative:
        file_global = folder_to / file
        if debug > 2: print("checking file", file_global)
        if file_global.exists():
            if debug > 2: print("FILE EXISTS!")
            result = False
            conflicting_files.append(file)
        elif debug > 2: print("Not exists")

    return result, conflicting_files



class mod_knowledge_base():
    def __init__(self, mode: str = "r"):
        # load KB
        self.dic = file_man.get_dict()
        self.mode = mode


    def __enter__(self):
        return self


    def __exit__(self, exc_type, exc_value, traceback):
        if debug > 3: print("EXIT", exc_type, exc_value, traceback)
        # save knowledge
        if self.mode == "w":
            file_man.save_dict(self.dic)


    def add_file(self, file_relative_path: Path, mod_name: str, file_version = None, date: datetime = None):
        # file version is used to determine which mod actually installed this file
        # or is it a patch for several mods
        if self.mode != "w":
            raise RuntimeError("Can't save knowledge without write permission.")

        # didn't find a better way to avoid KeyError if it's the first time the file is installed
        if not str(file_relative_path) in self.dic["installed_files"]:
            self.dic["installed_files"][str(file_relative_path)] = {"mods": []}
        self.dic["installed_files"][str(file_relative_path)]["mods"].append( mod_name )
        self.dic["installed_files"][str(file_relative_path)]["installed"] = str(datetime.now())
        if file_version:
            self.dic["installed_files"][str(file_relative_path)]["file_version"] = file_version



    def check_mod_installed(self, mod_name: str) -> bool:
        mod_list = self.get_installed_mods()
        return mod_name in mod_list


    def check_file_has_single_mod(self, file_relative_path: Path) -> bool:
        if debug > 2:
            print ("check_file_has_single_mod")
            print (file_relative_path)
        result = len(self.dic["installed_files"][file_relative_path]["mods"]) == 1
        if debug > 2: print(result)
        return result


    def get_all_files(self):
        return self.dic["installed_files"].keys()


    def get_file_version(self, relative_file_path: Path):
        file_info_dict = self.dic["installed_files"].get(str(relative_file_path), {})
        return file_info_dict.get("file_version", "")

    def get_files_for_mod(self, mod_name: str) -> list[Path]:
        return [k for k, v in self.dic["installed_files"].items() if mod_name in v["mods"]]


    def get_mods_for_file(self, relative_file_path: Path) -> list[str]:
        if debug > 2: print("get_mods_for_file", relative_file_path)
        file_info_dict = self.dic["installed_files"].get(str(relative_file_path), {})
        if debug > 3: print("file_info_dict", file_info_dict)
        if debug > 3: print("mods", file_info_dict.get("mods", []))
        return file_info_dict.get("mods", [])

    
    def get_installed_mods(self):
        return self.dic["installed_mods"]


    def register_mod_as_installed(self, mod_name):
        if self.mode != "w":
            raise RuntimeError("Can't save knowledge without write permission.")
        self.dic["installed_mods"].append(mod_name)


    def remove_file_completely(self, file_relative_path: Path):
        if self.mode != "w":
            raise RuntimeError("Can't save knowledge without write permission.")
        del self.dic["installed_files"][file_relative_path]


    def remove_mod_from_file(self, file_relative_path: Path, mod_name: str):
        if self.mode != "w":
            raise RuntimeError("Can't save knowledge without write permission.")
        if self.check_file_has_single_mod(file_relative_path):
            self.remove_file_completely(file_relative_path)
        else:
            self.dic["installed_files"][file_relative_path]["mods"].remove(mod_name)


    def unregister_mod_from_installed_without_touching_files(self, mod_name):
        if self.mode != "w":
            raise RuntimeError("Can't save knowledge without write permission.")
        self.dic["installed_mods"].remove(mod_name)



class manager_installed_mods():
    def compare_files_and_knowledge(self, file_man: file_manager):
        # TODO: change all this file_man thing, it looks like crap on a piano
        # and maybe even separate this in two parts.. maybe. But it may also be to much
        knowledge_files = mod_knowledge_base().get_all_files() # relative, strings
        existing_files = file_man.get_all_files(settings_gamedata_folder) # absolute, Paths

        for file in knowledge_files:
            if ( settings_gamedata_folder / file ) not in existing_files:
                print("File not exists:", file)

        for file in existing_files:
            file = str( Path(file).relative_to(settings_gamedata_folder) )
            if file not in knowledge_files:
                print("File not in knowledge:", file)



    def interactive_folder_selection(self, extraction_path: Path, mod_name: str):
        option_install = ">> Install from here <<"
        option_end = "<< Complete Installation >>"

        something_installed = False

        print("Interactive mod installation started")
        print("Please enter gamedata folder and select 'install")
        print("Select Quit to stop installation")

        current_path = extraction_path

        interactive = True
        while interactive:
            print()
            print(fat_separator)
            print(current_path.relative_to(settings_temp_dir))
            print(thin_separator)
            dir_list = [".."]
            for item in current_path.iterdir():
                if item.is_dir():
                    dir_list.append(item)
            dir_list.append(option_install)
            dir_list.append(option_end)

            selection = select_item_from_list(dir_list, "choose", "folder")

            if selection == "XX ERROR XX":
                pass
            if selection == option_end:
                interactive = False
                break
            elif selection == option_install:
                print("Will install from folder", current_path)
                _, files_relative = get_files(current_path)
                self.move_files(current_path, settings_gamedata_folder, files_relative, mod_name)
                print("Installed!")
                # still flag as installed, so that it was displayed in list of installed mods
                something_installed = True
            else:
                current_path = (current_path / selection).resolve()

        return something_installed




    def install_new_mod(self, mod_archive_path: Path, mod_name: str, force = False):
        # 1. check if it's installed
        # 1.1. have force mode
        # 2. unpack
        # 3. parse
        # 4. display and ask which to install
        # 5. check file conflicts if it's not installed
        # 6. install
        #
        # 3-6 can be done interactively, by the way
        # won't need to use fomod at all!
        #
        
        is_mod_installed = mod_knowledge_base().check_mod_installed(mod_name)
        if not force and is_mod_installed:
            print("Mod is already installed in the list. Skipping.")
            print("Use 'force' flag to skipp the check")
        else:
            print("Staring file installation for mod", mod_archive_path.name)
            # 2. unpack
            extraction_dir = settings_temp_dir / mod_archive_path.name
            archive().extract(mod_archive_path, extraction_dir, debug)
            # parse (currently just find gamedata)
            something_installed = self.interactive_folder_selection(extraction_dir, mod_archive_path.name) # and installation

            if something_installed:
                with mod_knowledge_base("w") as mkb:
                    mkb.register_mod_as_installed(mod_name)
                print("Installed!")


    def move_files(self, path_from: Path, path_to: Path, file_list_relative: list[Path], mod_name: str):
        def create_folder_for(f: Path):
            if not f.parent.parent.exists():
                create_folder_for(f.parent)
            if not f.parent.exists():
                if debug > 1: print("created folder", f.parent)
                f.parent.mkdir()

        if debug > 0: print("Moving files")
        if debug > 1:
            print("Files to move list:")
            for f in Path(path_from).rglob("*"):
                print("   ", f)
        with mod_knowledge_base("w") as mkb:
            for f in file_list_relative:
                # check if the file already installed,
                # list mods, let user decide whether to
                # replace, skip or patch the file
                # TODO: automatic patch application
                installed_mods = mkb.get_mods_for_file(f)
                if debug > 2:
                    print(f)
                    print(installed_mods)

                overwrite = False
                answer = "wrong_answer =P"
                file_version = mod_name
                if len(installed_mods) > 0:
                    file_version = mkb.get_file_version(f)
                    print("File already installed:")
                    print(f)
                    print("by mod/patch:", file_version)
                    print("Do you want to over[w]rite, [k]eep existing or patch [m]anually?")
                    print("NEED TO ADD AN OPTION FOR INSTALLED MOD TO CONTAIN A PATCH") #TODO some day later
                    while answer not in "wkm":
                        answer = input("[w, k, m] > ")
                        if debug > 1: print("answer is", answer)
                    if answer == "w":
                        overwrite = True
                        file_version = mod_name
                    elif answer == "m":
                        print("Please replace the file with a patch now.")
                        print("I advice you to keep the old file in case you decide to remove this mod later.")
                        print("Later I will make some sort of autopatching mechanism")
                        input("Then press Enter")
                        file_version = mod_name + "_PATCH_OVER_" + file_version
                else:
                    # TODO: check for file conflicts here, because maybe the file is just not in the knowledge
                    print("No mods for")
                    print(f)
                    overwrite = True
                # create a knowledge that file is moved
                mkb.add_file(f, mod_name, file_version = file_version)
                # check destination file folder exists
                create_folder_for(path_to / f)
                # move the file
                # source.replace(destination)
                if overwrite:
                    if debug > 1:
                        print("FROM", ( path_from / f ))
                        print("` TO", (path_to / f))
                    ( path_from / f ).replace(path_to / f)


    def remove(self, mod_name: str):
        self.remove_files(mod_name)
        with mod_knowledge_base("w") as mkb:
            mkb.unregister_mod_from_installed_without_touching_files(mod_name)
        print("Removed!")

    def remove_files(self, mod_name: str):
        skip_not_found = False
        def remove_file(relative_file_path: Path, skip_not_found: bool):
            # remove file iteself
            print("DEL", (settings_gamedata_folder / relative_file_path))
            try:
                (settings_gamedata_folder / relative_file_path).unlink()
            except FileNotFoundError as error:
                print(error)
                if not skip_not_found:
                    print("Warning! A file from the list was not found.")
                    print("Do you want to remove it from list and continue?")
                    reply = input("[y]es, [n]o, [a]ll > ")

                    if reply in "all":
                        skip_not_found = True
                    elif reply in "yes":
                        pass
                    else:
                        raise FileNotFoundError(error)
            
            return skip_not_found

        # load file knowledge
        with mod_knowledge_base("w") as mkb:
            # remove known files:
            # 1. Check if several mods use this file
            # 2. Ask what to do (provide info)
            # ".copy()", coz dict should not change during iterations
            file_list = mkb.get_files_for_mod(mod_name)
            for file_relative_path in file_list:
                # check if other mods use this file:
                if not mkb.check_file_has_single_mod(file_relative_path):
                    # ask what do to
                    # ToDo replace with automatic restoration of a previous version of the file if possible
                    # or check for another existing merged version
                    print("More than one mod is using this file:")
                    print(file_relative_path)
                    print(mkb.get_mods_for_file(file_relative_path))
                    print("Current version is:", mkb.get_file_version(file_relative_path))
                    print("[K]eep the file or [d]elete?")

                    # get the right answer from the user
                    # can't trust no one with those things..
                    answer = "not in kd" # code pun, you found it
                    while answer not in "kd": # yea, lazy variant, with "kd" doing nothing
                        answer = input("[k/d] > ")
                        if answer == "k":
                            print("Keeping the file, but removing the reference in the knowledge")
                            # remove reference to this mod from file knowledge
                            mkb.remove_mod_from_file(file_relative_path, mod_name)
                        elif answer == "d":
                            print("Removing the file and the reference")
                            skip_not_found = remove_file(file_relative_path, skip_not_found)
                            mkb.remove_file_completely(file_relative_path)

                else:
                    # if it is the last mod that uses this file:
                    skip_not_found = remove_file(file_relative_path, skip_not_found)
                    mkb.remove_mod_from_file(file_relative_path, mod_name)

        file_man.remove_empty_dirs(settings_gamedata_folder)


    def test_files_no_install(self, mod_archive_path: Path):
        print("DOESN'T WORK! NEED TO SELECT GAMEDATA SOMEHOW FIRST")
        raise NotImplementedError("not yet")
        # 2. unpack
        extraction_dir = settings_temp_dir / mod_archive_path.name
        archive().extract(mod_archive_path, extraction_dir, debug)
        files_relative = file_man.get_all_files(extraction_dir)
        files_relative = [f.relative_to(extraction_dir) for f in files_relative]
        if debug > 1:
            for file in files_relative:
                print(file)
        bool_no_conflicts, list_conflicts = check_no_file_conflicts_old_code_using_to_test_without_install(settings_gamedata_folder, files_relative)
        if not bool_no_conflicts:
            print("File conflicts:")
            for f in list_conflicts:
                with mod_knowledge_base() as mkb:
                    names = mkb.get_mods_for_file(f)
                print(f)
                for name in names:
                    print(" >", name)
        else:
            print("No file conflicts found!")

        


def get_files(from_folder_path: Path):
    # find files in gamedata:
    # DEBUG
    # print(type(mod_settings_gamedata_folder))
    # print(mod_settings_gamedata_folder)
    # print("All:")
    # for x in mod_settings_gamedata_folder.iterdir():
    #     print(" >", x)
    # print("File list:")
    files = [ file for file in from_folder_path.rglob("*") if file.is_file() ]
    # DEBUG
    # for f in files:
    #     print(" >", f)
    # print("end file list")
    files_relative = [ file.relative_to(from_folder_path) for file in files ]
    return files, files_relative


def parse_unpacked_mod(extraction_dir: str, archive_name: str = None):
    # this is using predefined configuration to make things "easier" for the end user
    # BUT I've decided that it's not a direction I want.
    # I will intead interactively go through the unpacked archive and let the user to select which folder to install

    # find gamedata folder, additional folders etc...
    # still not working with additional folders, and flow is not compatible!


    extraction_path = Path(extraction_dir)

    # first, should check if there is an existing system.
    # but since I start with unsupported, I make mine

    # check if passed archive name is in supported archives list
    # if not, try searching for existing file manager systems
    # if not, just try to copy gamedata
    if archive_name:
        conf_file_name = archive_name + ".yaml"
        #print(conf_file_name)
        #print(settings_mods_to_install_folder)
        conf_file_path = settings_conf_folder / conf_file_name

        if conf_file_path.exists():
            # find gamedata folder:
            configs = yaml_reader(conf_file_path).to_dict()
            warnings = configs.get("warnings", None)
            if warnings:
                print()
                print("WARNING".center(decorations_width, "!"))
                print("\n", warnings)
                input("press Enter to continue")
            mod_gamedata_folder = extraction_path / configs["gamedata_location"]
            return mod_gamedata_folder, *get_files(mod_gamedata_folder)
            

    if False:
        # check for known modmamaging metafiles here
        raise NotImplementedError()
    elif (extraction_path / "gamedata").exists():
        print("No supported archive structure found, trying to just cope gamedata.")
        print("This is fine, for mods that don't have conflicts or installed on top.")
        mod_gamedata_folder = extraction_path / "gamedata"
        return mod_gamedata_folder, *get_files(mod_gamedata_folder)
    else:
        raise Exception("Unsupported archive structure")


def get_fomod_data(root: Path):
    # TODO:
    # 1. deal with main data
    # 2. deal with optional data
    # 2.1. show prompts and install
    # 3. deal with orders and requirements
    # 4. possibly, switch to fomod istead of my structure
    pass


def main_menu():
    menu_choices = """
1: [C]ompare installed and known files
2: [I]nstall single mod
3: [R]emove single mod
4: Check installed mod [l]ist
5: [T]est file conflicts without installing
0: [Q]uit

> """
    print(to_do)
    print("\n")
    print(" MAIN MENU ".center(decorations_width, "="))
    return input(menu_choices)

def get_packages_path_list() -> list[Path]:
    return [f for f in settings_mods_to_install_folder.iterdir()]


def select_item_from_list(input_list: list, action="install", item_name = "mod"):
    n = 0
    item = "XX ERROR XX"
    selected = False
    while not selected: # crude foolproofing, should rework, replace, TODO
        for i in input_list:
            if type(i) == str:
                pass
            else: # path pr windows path
                i = i.name

            print("  " + str(n) + ": " + i)
            n += 1
        
        selection = int(input("\nSelect number to "+ action + " a " + item_name + "\n> "))

        if selection >= len(input_list):
                print("\nNo " + item_name + " with selected number!")
        else:
            selected = True
            item = input_list[selection]

    return item
    


# ================================================================
#      START
# ================================================================


if debug > 0: print("Init...")
m_i_m = manager_installed_mods()
file_man = file_manager(settings_manager_installed_mods_file)
#upgrade
patcher(file_man).upgrade_knowledge()
#cleanup
if settings_temp_dir.exists():
    file_man.rm_rf(settings_temp_dir)
if debug > 0: print("Done!")

while True:
    selection = main_menu()

    if selection in "Qq0":
        break

    elif selection in "Cc1":
        m_i_m.compare_files_and_knowledge(file_man)

    elif selection in "iI2":
        print("\nPackages in the input folder:")
        l = get_packages_path_list()
        i = select_item_from_list(l)
        m_i_m.install_new_mod(i, i.name)


    elif selection in "rR3":
        print("Installed mods:")
        # re-check new mods every time!
        # don't load it once and use then
        with mod_knowledge_base() as mkb:
            l = mkb.get_installed_mods()
        i = select_item_from_list(l, action="remove")
        m_i_m.remove(i)

    elif selection in "Ll4":
        print("Installed mods:")
        with mod_knowledge_base() as mkb:
            l = mkb.get_installed_mods()
        for i in l:
            print(i)

    elif selection in "Tt5":
        print("\nPackages in the input folder:")
        l = get_packages_path_list()
        i = select_item_from_list(l, "check conflicts with")
        m_i_m.test_files_no_install(i)


if settings_temp_dir.exists():
    file_man.rm_rf(settings_temp_dir)