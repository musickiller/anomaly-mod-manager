# My very small yaml implementation
# so that you won't need to install an extra dependency

from pathlib import Path

class yaml_reader():
    def __init__(self, file_path: Path) -> None:
        self.dic = {}

        with open(file_path, "r") as file_handler:
            yaml_lines = file_handler.readlines()
            for line in yaml_lines:
                s = line.rstrip("\n").split(":")
                self.dic[s[0]] = s[1].lstrip(" ")
    
    def to_dict(self) -> dict:
        return self.dic